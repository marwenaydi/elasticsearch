import elasticsearch
from elasticsearch_dsl import Search
import pathlib


INDEX_NAME='index-4'

ELASTIC_HOST='http://localhost:9200/'

client = elasticsearch.Elasticsearch(hosts=[ELASTIC_HOST])

data_1={
    'id':1,
    'name':'marwen_aydi',
    'tag':'python',
}
    
data_2={
    'id':2,
    'name':'azer',
    'tag':'java',
    
}
""" add_data_1=client.index(index=INDEX_NAME,body=data_1)
print(add_data_1)
add_data_2=client.index(index=INDEX_NAME,body=data_2)
print(add_data_2) """

data_3={
    'id':3,
    'name':'es',
    'tag':pathlib.Path('es.txt').read_text()

}
#add_data_5=client.index(index=INDEX_NAME,body=data_3)
if __name__=="__main__":
    q=input('what do you want \n')
    field = ['name', 'tag']
    results=Search(
        index=INDEX_NAME).using(client).query("multi_match",
        fields=field, fuzziness='AUTO', query=q)

for hit in results:
    print(hit.id)
    print(hit.name)

