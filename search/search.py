import elasticsearch
from elasticsearch_dsl import Search
ELASTIC_HOST='http://localhost:9200/'
client = elasticsearch.Elasticsearch(hosts=[ELASTIC_HOST])
INDEXES=['products']
def lookup(query,index=INDEXES,fileds=['title','description'],):
    if not query:
        return None
    results=Search(
        index=INDEXES).using(client).query("multi_match",
        fields=fileds ,query=query)
    result=[]
    for hit in results:
       data={
           "title":hit.title,
           "description":hit.description,
           "url":hit.url,
       }
       result.append(data)
    return result
