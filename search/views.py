from django.shortcuts import render
from elasticsearch_dsl import search
from .search import lookup
# Create your views here.

def search_views(request):
    query_params=request.GET
    q=query_params.get('q')
    context={}
    if q is not None:
        results =lookup(q)
        context['results']=results
        context['query']=q
    return render(request,'index.html',context)